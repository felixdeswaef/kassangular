import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Fooditem} from './fooditem';
import {Foodplace} from './foodplace';
import {Billitem} from './billitem';
import {Foodbills} from './foodbills';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})

};

@Injectable()
export class ApiService {
  private base = 'http://felixdeswaef.be';

  constructor(private http: HttpClient) {


  }

  // Uses http.get() to load data from a single API endpoint
  getFoods() {
    return this.http.get(this.base + '/api/fitem');
  }

  delFood(id: number) {
    return this.http.delete(this.base + '/api/fitem/' + id);
  }

  getFood(id: number) {
    return this.http.get(this.base + '/api/fitem/' + id);
  }

  addFood(item: Fooditem) {
    return this.http.put(this.base + '/api/fitem', item);
  }


  getPlaces() {
    return this.http.get(this.base + '/api/fplace');
  }

  delPlace(id: number) {
    return this.http.delete(this.base + '/api/fplace/' + id);
  }

  getPlace(id: number) {
    return this.http.get(this.base + '/api/fplace/' + id);
  }

  addPlace(item: Foodplace) {

    return this.http.post(this.base + '/api/fplace', item);
  }


  getBills() {
    return this.http.get(this.base + '/api/fbill');
  }

  delBill(id: number) {
    return this.http.delete(this.base + '/api/fbill/' + id);
  }

  getBill(id: number) {
    return this.http.get(this.base + '/api/fbill/' + id);
  }

  addBill(item: Foodbills) {

    return this.http.post(this.base + '/api/fbill', item);
  }


  getLines() {
    return this.http.get(this.base + '/api/fbitem');
  }

  updLine(id: number, item: Billitem) {
    return this.http.put(this.base + '/api/fbitem/' + id, item);
  }

  delLine(id: number) {
    return this.http.delete(this.base + '/api/fbitem/' + id);
  }

  getLine(id: number) {
    return this.http.get(this.base + '/api/fbitem/' + id);
  }

  addLines(item: Billitem) {

    return this.http.post(this.base + '/api/fbitem', item);
  }
}
