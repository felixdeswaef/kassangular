import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {SidenavComponent} from './sidenav/sidenav.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {TestcompComponent} from './testcomp/testcomp.component';
import {ApiService} from './api.service';
import {PlaceFormComponent} from './place-form/place-form.component';
import {RouterModule, Routes} from '@angular/router';
import {PlacePrintComponent} from './place-print/place-print.component';
import { PlaceIndexComponent } from './place-index/place-index.component';
import { OrdererComponent } from './orderer/orderer.component';
import { FoodComponent } from './food/food.component';
import { BillselectorComponent } from './billselector/billselector.component';
import { DashComponent } from './dash/dash.component';
import { NamefilterPipe } from './namefilter.pipe';

const AppRoutes: Routes = [
  {path: '', component: BillselectorComponent},
  {path: 'place', component: PlaceIndexComponent},
  {path: 'place/new', component: PlaceFormComponent},
  {path: 'place/:ID', component: PlacePrintComponent},
  {path: 'place/:ID/edit', component: PlaceFormComponent},


  {path: 'food', component: BillselectorComponent},
  {path: 'food/:BILL', component: BillselectorComponent},
  {path: 'dash', component: DashComponent}


];

@NgModule({
  declarations: [
    AppComponent,
    SidenavComponent,
    TestcompComponent,
    PlaceFormComponent,
    PlacePrintComponent,
    PlaceIndexComponent,
    OrdererComponent,
    FoodComponent,
    BillselectorComponent,
    DashComponent,
    NamefilterPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(AppRoutes),
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
