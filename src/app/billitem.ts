export class Billitem {
  id: number;
  subselect: string;
  username: string;
  count: number;
  billid: number;
  itemid: number;
  userid: number;

}
