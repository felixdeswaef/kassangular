import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillselectorComponent } from './billselector.component';

describe('BillselectorComponent', () => {
  let component: BillselectorComponent;
  let fixture: ComponentFixture<BillselectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillselectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillselectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
