import {Component, OnInit} from '@angular/core';
import {DataconService} from '../datacon.service';

@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.css']
})
export class DashComponent implements OnInit {

  constructor(public dc: DataconService) {
  }

  ngOnInit() {
  }

}
