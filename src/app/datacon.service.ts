import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Fooditem} from './fooditem';
import {Foodplace} from './foodplace';
import {Foodbills} from './foodbills';
import {Card} from './card';
import {debug, log} from 'util';
import {Billitem} from './billitem';
import {ActivatedRoute, Route, Router, RouterLinkActive} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class DataconService {
  foods: Fooditem[];
  places: Foodplace[];
  bills: Foodbills[];
  lines: Billitem[];
  stack: Card[];
  usernames: string[];
  card: Card;
  username = 'demouser';
  vusername: string;
  plready = false;
  fiready = false;
  fbready = false;
  biready = false;
  sumcnt: number;
  sumval: number;
  title = '';
  prev = 'prev';
  next = 'next';
  foodstate = 1; // 0 - none 1 - make bill 2 - foodlist 3-
  // unused  billstate = null; // null none selected 1- bill selected 2- new bill in the making
  selbilid = null;
  bill: Foodbills;

  constructor(public api: ApiService, private router: Router, private route: ActivatedRoute) {

    this.update();
    this.vusername = this.username;

  }

  prevb() {
    switch (this.foodstate) {
      case 4:
        this.foodstate = 3;
        break;
      case 3:
        this.foodstate = 2;
        break;

    }
    this.refresh();
  }

  nextb() {
    switch (this.foodstate) {
      case 3:
        this.foodstate = 4;
        break;
      case 2:
        this.foodstate = 3;
        break;

    }
    this.refresh();
  }

  readyC() {
    if (this.plready && this.fiready && this.biready && this.fbready) {
      this.refresh();

    }
  }

  updFoods() {
    this.api.getFoods().subscribe((response: Fooditem[]) => {

      this.foods = response;
      this.fiready = true;
      this.readyC();

    });
  }

  updPlaces() {
    this.api.getPlaces().subscribe((response: Foodplace[]) => {

      this.places = response;
      this.plready = true;
      this.readyC();
    });
  }

  updBills() {
    this.api.getBills().subscribe((response: Foodbills[]) => {

      this.bills = response;
      this.fbready = true;
      this.readyC();

    });
  }

  updBilllin() {
    this.api.getLines().subscribe((response: Billitem[]) => {

      this.lines = response;
      this.biready = true;
      this.readyC();
    });
  }

  refresh() {

    const tempcardstack = [];

    log('refreshing' + this.foodstate);
    let temp = [];

    this.sumcnt = 0;
    this.sumval = 0;
    this.bill = this.bills.find(this.matchid(this.selbilid));


    switch (this.foodstate) {
      case 1:
        this.title = 'New Bill';
        this.places.forEach((elem: Foodplace) => {
          let nc = new Card();
          nc.title = elem.name;
          nc.id = elem.id;


          tempcardstack.push(nc);

          nc = null;


        });
        break;
        break;
      case 2:
        if (this.foods == null) {
          break;
        }
        this.title = 'Select Food';

        this.next = 'Show Total';
        temp = [];

        temp.push(this.foods.filter(this.matchplaceid(this.bill.placeid)));
        log(temp[0]);
        temp[0].forEach((elem: Fooditem) => {
          let nc = new Card();
          nc.title = elem.name;
          nc.id = elem.id;
          nc.price = elem.price;
          const tmp2 = this.lines.find(this.matchusername(this.username, elem.id));
          if (tmp2 != null) {
            nc.rbox = tmp2.count.toString();
          } else {
            nc.rbox = '0';
          }


          tempcardstack.push(nc);

          nc = null;


        });
        break;
      case 3:
        if (this.foods == null) {
          break;
        }

        this.title = 'Personal Total For';
        this.prev = 'change order';
        this.next = 'show order total';
        temp = [];

        temp.push(this.foods.filter(this.matchplaceid(this.bill.placeid)));
        log(temp[0]);
        const tusers = [];
        this.lines.filter(this.matchanybill()).forEach((elem: Billitem) => {
          if (!tusers.includes(elem.username)) {
            tusers.push(elem.username);
          }
        });
        this.usernames = tusers;


        temp[0].forEach((elem: Fooditem) => {
          let nc = new Card();
          nc.title = elem.name;
          nc.id = elem.id;
          nc.price = elem.price;

          const tmp2 = this.lines.find(this.matchusername(this.vusername, elem.id));
          if (tmp2 != null) {
            nc.rbox = tmp2.count.toString();
          } else {
            nc.rbox = '0';
            return;
          }
          nc.sub2 = '';
          this.sumcnt += tmp2.count;
          this.sumval += (elem.price * tmp2.count);
          nc.sub1 = (elem.price * tmp2.count).toString();

          if (tmp2.count > 0) {

            tempcardstack.push(nc);
          }
          nc = null;


        });
        break;
      case 4:
        if (this.foods == null) {
          break;
        }
        this.title = 'Order Total';
        this.prev = 'show personal total';
        temp = [];

        temp.push(this.foods.filter(this.matchplaceid(this.bill.placeid)));
        log(temp[0]);

        temp[0].forEach((elem: Fooditem) => {
          let nc = new Card();
          nc.title = elem.name;
          nc.id = elem.id;
          nc.price = elem.price;
          let count = 0;

          this.lines.filter(this.matchanyusername(elem.id)).forEach((tmp2: Billitem) => {


            log(tmp2);
            count += tmp2.count;


          });
          this.sumcnt += count;
          this.sumval += (elem.price * count);
          nc.rbox = count.toString();
          nc.sub1 = (elem.price * count).toString();
          if (count > 0) {

            tempcardstack.push(nc);
          }
          nc = null;
        });
        break;

    }
    this.stack = tempcardstack;
    // log(this.stack, tempcardstack);
  }

  matchid(ids) {
    return ((element) => {

      return element.id === ids;
    });
  }

  matchplaceid(ids) {
    return ((element) => {

      return element.placeid === ids;
    });
  }

  matchusername(uname, id) {
    return ((element) => {

      return element.username === uname && element.itemid === id && element.billid === this.selbilid;
    });
  }

  matchanyusername(id) {
    return ((element) => {

      return element.itemid === id && element.billid === this.selbilid;
    });
  }

  matchanybill() {
    return ((element) => {

      return element.billid === this.selbilid;
    });
  }

  changebill(id: number) {
    this.vusername = this.username;
    log(id + ' ' + this.foodstate);
    if (id == null) {
      log('nullbillid make new one');
      this.selbilid = null;
      this.foodstate = 1;
      this.refresh();
    } else {
      this.selbilid = id;
      this.upstep(null);
      this.refresh();
    }

  }

  upstep(id: number) {
    if (this.foodstate === 1) {
      this.foodstate = 2;
    } else {
      log(id + ' ' + this.foodstate);
    }


  }

  addc(id: number, cnt: number) {
    const tmp = this.lines.find(this.matchusername(this.username, id));
    if (tmp == null) {
      log('tmpisnull');
      const bi = new Billitem();
      bi.billid = this.selbilid;
      bi.itemid = id;
      bi.count = 1;
      bi.subselect = 'No Options';
      bi.username = this.username;
      bi.userid = 0;
      log(bi);
      this.api.addLines(bi).subscribe((response) => {
          log(response);

          log(bi);
          this.updBilllin();

        }
      );
    } else {
      log('tmpnotnull');
      log(tmp);
      const bi = new Billitem();
      bi.billid = this.selbilid;
      bi.itemid = id;
      bi.count = cnt + 1;
      bi.subselect = 'No Options';
      bi.username = this.username;
      bi.userid = 0;

      this.api.updLine(tmp.id, bi).subscribe((response) => {
          log(response);


          this.updBilllin();

        }
      );
    }

  }

  remc(id: number, cnt: number) {
    const tmp = this.lines.find(this.matchusername(this.username, id));
    log('tmpnotnull');
    if (tmp == null) {

    } else if (cnt > 0) {
      // log("tmpnotnull");
      log(tmp);
      const bi = new Billitem();
      bi.billid = this.selbilid;
      bi.itemid = id;
      bi.count = cnt - 1;
      bi.subselect = 'No Options';
      bi.username = this.username;
      bi.userid = 0;

      this.api.updLine(tmp.id, bi).subscribe((response) => {
          log(response);


          this.updBilllin();

        }
      );
    }
  }


  update() {

    this.updPlaces();
    this.updFoods();
    this.updBills();
    this.updBilllin();


  }

  placecards(): Card[] {
    if (this.plready) {
      log(this.places);
      this.stack = null;
      this.places.forEach((place: Foodplace) => {

        this.card = new Card();
        this.card.title = place.name;
        this.card.id = place.id;
        this.card.sub2 = place.phone;
        this.card.sub1 = place.type;

        this.stack.push(this.card);
      });
      return this.stack;
    }
  }

  delplace(id: number) {
    this.api.delPlace(id).subscribe((result) => {
      log(result);
      this.router.navigate(['/dash']);
      this.update();
    });
  }

}

