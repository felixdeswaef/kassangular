import {Component, OnInit} from '@angular/core';
import {ApiService} from '../api.service';
import {DataconService} from '../datacon.service';
import {Billitem} from '../billitem';
import {Foodbills} from '../foodbills';
import {Fooditem} from '../fooditem';
import {log} from 'util';
import {Resp} from '../resp';

@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.css']
})
export class FoodComponent implements OnInit {
  NewBillName: string;
  NewBillPlace: number;

  constructor(public dc: DataconService) {
  }


  addbill() {
    const bi: Foodbills = new Foodbills();
    bi.name = this.NewBillName;
    bi.placeid = this.NewBillPlace;

    this.dc.api.addBill(bi).subscribe((response: Resp) => {
      const n = (response.task.id);
      this.dc.selbilid = n;

      this.dc.updBills();
      this.dc.upstep(null);
    });
  }

  ngOnInit() {

  }

}
