export class Fooditem {
  id: number;
  name: string;
  price: number;
  placeid: number;
}
