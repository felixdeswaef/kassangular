import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'namefilter'
})
export class NamefilterPipe implements PipeTransform {

  transform(value: string, args?: any): any {
    value = value.replace(/ /, '_');

    value = value.replace(/toon/i, 'Loser');
    value = value.toLocaleLowerCase();
    if (value.length > 10) {
      value = value.slice(0, 9);
      value.bold();
    }
    return value;
  }

}
