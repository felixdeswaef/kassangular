import {Component, OnInit} from '@angular/core';
import {ApiService} from '../api.service';
import {ActivatedRoute, Params} from '@angular/router';
import {Foodbills} from '../foodbills';
import {Card} from '../card';
import {Fooditem} from '../fooditem';
import {DataconService} from '../datacon.service';
import {log} from 'util';
import {promise} from 'selenium-webdriver';
import delayed = promise.delayed;

@Component({
  selector: 'app-orderer',
  templateUrl: './orderer.component.html',
  styleUrls: ['./orderer.component.css']
})
export class OrdererComponent implements OnInit {
  step: number;
  bill: Foodbills;
  cardstack: Card[];

  constructor(private api: ApiService, private dc: DataconService, private route: ActivatedRoute) {
  }

  validatestep() {
    switch (this.step) {
      case 4 :
      case 3:
      case 2:
        if (this.bill == null) {
          this.step = 0;
        }
    }
    log('updating stack');

    if (this.dc.plready) {
      this.updatecardstack();
    }

  }

  updatecardstack() {
    log(this.step);
    switch (this.step) {
      case 0 :/*0 for billselec 1 for newbil placeselect 2 for foodselect 3for bill display 4 for personal sumary*/
        break;
      case 1:
        this.cardstack = this.dc.placecards();
        log('l55' + this.cardstack);
        break;
      case 2:

        break;
      case 3 :
        break;
      case 4 :
        break;
    }
  }

  register(id: number) {

  }

  // updwtloop() {
  //   delayed(300).then((value: void) => {
  //       if (this.dc.plready) {
  //         this.updatecardstack();
  //       } else {
  //         delayed(300).then((vabalue: void) => {
  //           this.updwtloop();
  //         });
  //       }
  //
  //     }
  //   );
  // }

  ngOnInit() {
    this.route.paramMap.subscribe((res: Params) => {
      if (res.get('STEP') != null) {

        this.step = (+res.get('STEP'));
        this.validatestep();
      } else {
        this.step = 0;
      }

    });
  }

}
