import {Component, OnInit} from '@angular/core';
import {Foodplace} from '../foodplace';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ApiService} from '../api.service';
import {log} from 'util';
import {DataconService} from '../datacon.service';

@Component({
  selector: 'app-place-form',
  templateUrl: './place-form.component.html',
  styleUrls: ['./place-form.component.css']
})
export class PlaceFormComponent implements OnInit {
  place: Foodplace;
  placename: string;
  placetype: string;
  placetel: string;
  newel = false;
  unexist = false;

  constructor(private dc: DataconService, private route: ActivatedRoute, private router: Router) {

  }

  updateornew() {
    if (this.newel) {
      this.place = new Foodplace();
      this.place.phone = this.placetel;
      this.place.type = this.placetype;
      this.place.name = this.placename;
      this.dc.api.addPlace(this.place).subscribe(
        (result) => {
          log(result);
        }, (error) => {
          log(error);
        });
    }
    this.dc.updPlaces();
    // this.router.navigate(['/dash']);
    window.location.assign('/dash'); // forces the refresh
  }

  ngOnInit() {

    this.route.paramMap.subscribe((params: Params) => {
      if (params.get('ID') != null) {
        this.dc.api.getPlace(+params.get('ID')).subscribe((result: Foodplace) => {
            log(result);
            this.place = result;
            this.placename = this.place.name;
            this.placetype = this.place.type;
            this.placetel = this.place.phone;
          },
          error => {
            this.unexist = true;
            log(error);
          }
        );
      } else {
        this.newel = true;
      }


    });


  }

}
