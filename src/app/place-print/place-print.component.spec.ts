import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlacePrintComponent } from './place-print.component';

describe('PlacePrintComponent', () => {
  let component: PlacePrintComponent;
  let fixture: ComponentFixture<PlacePrintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlacePrintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlacePrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
