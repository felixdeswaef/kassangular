import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {DataconService} from '../datacon.service';
import {Foodplace} from '../foodplace';
import {log} from 'util';

@Component({
  selector: 'app-place-print',
  templateUrl: './place-print.component.html',
  styleUrls: ['./place-print.component.css']
})
export class PlacePrintComponent implements OnInit {

  constructor(private route: ActivatedRoute, public dc: DataconService) {
  }

  myid: number;
  place: Foodplace;

  ngOnInit() {
    this.route.paramMap.subscribe((params: Params) => {
      if (params.get('ID') != null) {
        this.myid = +params.get('ID');
        this.getcontent();
      }
    });
  }


  getcontent() {
    this.dc.api.getPlace(this.myid).subscribe((result: Foodplace) => {
      log(result);
      this.place = result;
    });
  }

}
