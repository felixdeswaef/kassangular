import {Component, OnInit} from '@angular/core';
import {log} from 'util';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  cls = false;
  constructor() {
  }



  openc() {
    this.cls = false;
    log(this.cls);
  }

  closec() {
    this.cls = true;
    log(this.cls);
  }

  ngOnInit(): void {
    this.cls = false;
    log(this.cls);
  }
}
