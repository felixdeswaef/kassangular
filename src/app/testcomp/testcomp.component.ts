import {Component, OnInit} from '@angular/core';
import {ApiService} from '../api.service';
import {Foodplace} from '../foodplace';

@Component({
  selector: 'app-apiholder',
  templateUrl: './testcomp.component.html',
  styleUrls: ['./testcomp.component.css']
})
export class TestcompComponent implements OnInit {
  public foods: Foodplace[];

  constructor(private api: ApiService) {
  }

  ngOnInit() {
    this.foods = [];
    this.onGetFoods();
  }

  onGetFoods() {
    this.api.getFoods().subscribe(
      (response: Foodplace[]) => {
        console.log(response);
        this.foods = response;
      }
    );
  }
}
